from django.apps import AppConfig


class ContentAppConfig(AppConfig):
    name = "Content"
