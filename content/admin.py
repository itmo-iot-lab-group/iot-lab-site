from django.contrib import admin

from content.models import Project, MediaContent, GitUrl, Author


class ProjectAdmin(admin.ModelAdmin):
    fields = ('name', 'description')


class MediaContentAdmin(admin.ModelAdmin):
    fields = ('name', 'static_path', 'project')


class GitUrlAdmin(admin.ModelAdmin):
    fields = ('name', 'project')


class AuthorAdmin(admin.ModelAdmin):
    fields = ('name', 'project')


admin.site.register(Project)
admin.site.register(MediaContent)
admin.site.register(GitUrl)
admin.site.register(Author)
