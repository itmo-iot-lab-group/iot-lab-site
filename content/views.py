from django.views.generic import TemplateView

from content.models import get_all_projects


class ProjectsTemplateView(TemplateView):
    template_name = "content/projects.html"

    def get_context_data(self, **kwargs):
        context = super(ProjectsTemplateView, self).get_context_data(**kwargs)

        projects = get_all_projects()
        context['projects'] = projects

        return context
