from django.urls import path
from content.views import ProjectsTemplateView

urlpatterns = [
    path('view/', ProjectsTemplateView.as_view())
]
