from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=100, blank=False)
    description = models.CharField(max_length=1000, blank=True)

    class Meta:
        ordering = ['name']

    def set_media_content(self, media_content_list):
        self.media_content = media_content_list

    def set_git_urls(self, git_urls_list):
        self.git_urls = git_urls_list

    def set_authors(self, authors_list):
        self.authors = authors_list


class MediaContent(models.Model):
    name = models.CharField(max_length=100, blank=False)
    static_path = models.CharField(max_length=100, blank=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)


class GitUrl(models.Model):
    url = models.CharField(max_length=100, blank=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)


class Author(models.Model):
    name = models.CharField(max_length=100, blank=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)


def get_all_projects():
    projects = list(Project.objects.all())
    for project in projects:
        project.set_media_content(MediaContent.objects.filter(project=project))
        project.set_git_urls(GitUrl.objects.filter(project=project))
        project.set_authors(Author.objects.filter(project=project))

    return projects