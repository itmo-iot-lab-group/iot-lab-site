## IoT Lab Site

Authors: Egoshin & Kulinich

### Usage

1. Create migrations:
```bash
python manage.py makemigrations
python manage.py makemigrations content 
```
2. Apply migrations:
```bash
python manage.py migrate
python manage.py migrate content
```
3. Add superuser:
```bash
python manage.py createsuperuser
```
4. Run dev server:
```bash
python manage.py runserver
```